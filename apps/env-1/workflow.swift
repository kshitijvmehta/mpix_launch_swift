
import string;
import launch;

int procs[] = [ 3, 4];
string a[] = [ "var=3 printenv", "var=4 printenv" ];
@par=7 launch_multi(procs,
                    split("env env"),
                    a);
