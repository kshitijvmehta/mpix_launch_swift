#!/bin/sh
set -eu

PROCS=${1:-4}

MACHINE=${MACHINE:-}

THIS=$( dirname $0 )
LAUNCH=$( cd $THIS/../../src ; /bin/pwd )

set -x
stc -u -I $LAUNCH -r $LAUNCH workflow.swift
turbine -l -n $PROCS $MACHINE workflow.tic
