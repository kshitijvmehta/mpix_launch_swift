#!/bin/sh
set -eu

MACHINE=${MACHINE:-}

THIS=$( dirname $0 )
LAUNCH=$( cd $THIS/../../src ; /bin/pwd )

set -x
stc -u -I $LAUNCH -r $LAUNCH test-example-0.swift
turbine -n 4 $MACHINE test-example-0.tic
