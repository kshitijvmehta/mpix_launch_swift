
import io;
import launch;

string a[] = ["abc", "defg"];

program = "./example.x";
printf("swift: launching: %s", program);
exit_code = @par=3 launch(program, a);
printf("swift: received exit code: %d", exit_code);
if (exit_code != 0)
{
  printf("swift: The launched application did not succeed.");
}
