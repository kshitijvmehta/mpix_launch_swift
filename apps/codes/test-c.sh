#!/bin/sh
set -e

make test-c.x

export LD_LIBRARY_PATH=$HOME/codes-work/test-custom-comm/codes-install/lib:$HOME/codes-work/test-custom-comm/ROSS/install/lib:$HOME/codes-work/install/lib:$LD_LIBRARY_PATH

echo "run..."
./test-c.x
