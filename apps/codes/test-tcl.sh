#!/bin/sh
set -e

make

export LD_LIBRARY_PATH=$HOME/codes-work/test-custom-comm/codes-install/lib:$HOME/codes-work/test-custom-comm/ROSS/install/lib:$HOME/codes-work/install/lib:$LD_LIBRARY_PATH

TURBINE=$HOME/codes-work/test-custom-comm/swift-install/turbine
export TCLLIBPATH="$PWD $TURBINE/lib"
tclsh test.tcl
