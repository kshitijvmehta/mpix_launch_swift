
import string;
import launch;

int procs[] = [ 3, 4];
@par=7 launch_multi(procs, split("echo echo"),
                    split("hello1 hello2"));
