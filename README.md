
## Makefile

Build with `./build.sh`.  This allows you to put `mpicc` in your `PATH` *or*
set `CC` and `MPI`.

Be sure to put Turbine in your `PATH`.

On a Cray, build with something like:

~~~~
cd src
export CC=/opt/gcc/4.9.2/bin/gcc
export MPI=/opt/cray/mpt/default/gni/mpich2-gnu/49
export CFLAGS=-I$MPI/include
export LDFLAGS="-L$MPI/lib -lmpich"

PATH=/path/to/turbine/bin:$PATH

./build.sh
~~~~

Put this in a shell script for ease of use.
