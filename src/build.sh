#!/bin/bash
set -eu

# BUILD.SH

# Sets key environment variables for Makefile
# Passes arguments to Makefile
# You can either:
# 1. set CC to mpicc
# or
# 2. put mpicc in your PATH
# or
# 3a. set CC to gcc
# 3b. set MPI to the MPI installation (containing include/ and lib/)
# See the ../README

if [ ! -f "../mpix_launch/src/MPIX_Comm_launch.h" ]; then
cd ..
git submodule init
git submodule update
cd src
fi

export MPIX_LAUNCH=../mpix_launch

abort()
{
  echo $1
  exit 1
}

# Look up Swift/T and Tcl build settings
TURBINE=$( which turbine  ) || abort "Put turbine in your PATH!"
source $( $TURBINE -C )
export TCL_INCLUDE_SPEC

export BUILD_SH=1 # Signal that we are building from build.sh

if [[ ${CC:-} == "" ]]
then
  which mpicc 2>&1 > /dev/null || abort "Put mpicc in your PATH!"
  export CC=$( which mpicc )
fi
if [[ ${MPI:-} == "" ]]
then
  export MPI=$( dirname $( dirname $CC ) )
fi

# declare -p MPICC MPI TCL_INCLUDE_SPEC MPIX_LAUNCH

# Compile MPIX_Launch
cd ../mpix_launch
make ENABLE_SHARED=1
cd ../src

# Compile launch
make $*
