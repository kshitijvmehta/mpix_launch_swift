@par @dispatch=WORKER
(int status) launch(string cmd, string args[])
"launch" "0.0" "launch_tcl";

@par @dispatch=WORKER
  (int status) launch_multi(int procs[], string cmd[], string argv[])
"launch" "0.0" "launch_multi_tcl";
