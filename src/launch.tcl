
namespace eval launch {
  proc launch_tcl { outputs inputs args } {
    set exit_code [ lindex $outputs 0 ]
    rule $inputs "launch::launch_tcl_body $exit_code $inputs" \
        {*}$args type $turbine::WORK
  }
  proc launch_tcl_body { exit_code args } {
    # Unpack args TDs
    lassign $args cmd argv
    # Retrieve data
    set cmd_value [ turbine::retrieve_decr $cmd ]
    set length [ adlb::container_size $argv ]
    set tds    [ adlb::enumerate $argv dict all 0 ]
    # Receive MPI task information
    set comm   [ turbine::c::task_comm ]
    set rank   [ adlb::rank $comm ]
    # Construct a char**
    set charpp [ turbine::blob_strings_to_char_ptr_ptr $tds ]
    # Run the user code
    set exit_code_value [ launch $comm $cmd_value $length $charpp ]
    # Store result
    if { $rank == 0 } {
      store_integer $exit_code $exit_code_value
    }
  }

  proc launch_multi_tcl { outputs inputs args } {
    set exit_code [ lindex $outputs 0 ]
    rule $inputs "launch::launch_multi_tcl_body $exit_code $inputs" \
        {*}$args type $turbine::WORK
  }
  proc launch_multi_tcl_body { exit_code args } {
    # Unpack args TDs
    lassign $args procs cmd argv
	# puts $args
	set procs_dict [ adlb::enumerate $procs dict all 0 ]
	set count [ dict size $procs_dict ]
	# show count
	set cmd_dict [ adlb::enumerate $cmd dict all 0 ]
	set count2 [ dict size $cmd_dict ]
	if { $count != $count2 } {
	  turbine::turbine_error \
		  "launch_multi(): unequal counts in arguments!"
	}

	set argv_dict [ adlb::enumerate $argv dict all 0 ]
	set argv_size [ dict size $argv_dict ]
	set argc_dict [ dict create ]
	for { set i 0 } { $i < $argv_size } { incr i } {
	  dict set argc_dict $i 1
	}

	# Receive MPI task information
    set comm   [ turbine::c::task_comm ]
    set rank   [ adlb::rank $comm ]
    # Construct char**
	set procs_intpp [ turbine::blob_dict_to_int_array $procs_dict ]
	set cmd_charpp [ turbine::blob_strings_to_char_ptr_ptr $cmd_dict ]
	set argc_intpp [ turbine::blob_dict_to_int_array $argc_dict ]
	set argv_charpp [ turbine::blob_strings_to_char_ptr_ptr $argv_dict ]
    # Run the user code
    set exit_code_value \
		[ launch_multi $comm $count $procs_intpp \
			           $cmd_charpp $argc_intpp $argv_charpp]
    # Store result
    if { $rank == 0 } {
      store_integer $exit_code $exit_code_value
    }
  }

  proc launch_turbine_tcl { outputs inputs args } {
    set exit_code [ lindex $outputs 0 ]
    rule $inputs "launch::launch_turbine_tcl_body $exit_code $inputs" \
        {*}$args type $turbine::WORK
  }

  proc launch_turbine_tcl_body { exit_code args } {
    # Unpack args TDs
    lassign $args cmd argv
    # Retrieve data
    set cmd_value [ turbine::retrieve_decr $cmd ]
    set length [ adlb::container_size $argv ]
    set tds    [ adlb::enumerate $argv dict all 0 ]
    # Receive MPI task information
    set comm   [ turbine::c::task_comm ]
    set rank   [ adlb::rank $comm ]
    # Construct a char**
    set charpp [ turbine::blob_strings_to_char_ptr_ptr $tds ]
    # Run the user code
    set exit_code_value [ launch_turbine $comm $cmd_value $length $charpp ]
    # Store result
    if { $rank == 0 } {
      store_integer $exit_code $exit_code_value
    }
  }
}
