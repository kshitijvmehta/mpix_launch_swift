#ifndef LAUNCH_H
#define LAUNCH_H

#include <mpi.h>

int launch(MPI_Comm comm, char* cmd, int argc, char** argv);

int launch_turbine(MPI_Comm comm, char* cmd, int argc, char** argv);

int launch_multi(MPI_Comm comm, int count, int* procs,
                 char** cmd, int* argc, char** argv);

#endif
